import 'dotenv/config'
import test from './test.js'
import to100 from './0-100.js'

let data = []

if (process.env.PRODUCTION) {
  data = [
    ...to100
  ]
} else {
  data = [
    ...test
  ]
}

export default data
