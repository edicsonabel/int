export default [
  {
    slug: '0',
    title: 'Xiaomi Populele APP LED Bluetooth USB Smart Ukulele 1pc $85,99',
    description: 'Xiaomi Populele APP LED Bluetooth USB Smart Ukulele 1pc $85,99',
    link: 'https://www.gearbest.com/guitar/pp_1117365.html?lkid=79735527'
  },
  {
    slug: '1',
    title: 'Populele S1 Smart Ukulele for Beginner Adults $99,99',
    description: 'Populele S1 Smart Ukulele for Beginner Adults $99,99',
    link: 'https://www.gearbest.com/guitar/pp_1675431.html?lkid=79735533'
  },
  {
    slug: '2',
    title: 'Populele 2 LED Smart Ukulele for Beginners Bluetooth Guitar from Xiaomi youpin $109.99',
    description: 'Populele 2 LED Smart Ukulele for Beginners Bluetooth Guitar from Xiaomi youpin $109.99',
    link: 'https://www.gearbest.com/guitar/pp_009255517766.html?lkid=79735538'
  },
  {
    slug: '3',
    title: 'ENYAMUSIC U1K APP LED Bluetooth USB Smart 23 inch Ukulele Full Board $129.99',
    description: 'ENYAMUSIC U1K APP LED Bluetooth USB Smart 23 inch Ukulele Full Board $129.99',
    link: 'https://www.gearbest.com/guitar/pp_009371790530.html?lkid=79735546'
  },
  {
    slug: '4',
    title: 'SG106 Wi-Fi FPV RC Helicopter Quadcopter Drone with HD Camera Optical Flow Positioning $43.99',
    description: 'SG106 Wi-Fi FPV RC Helicopter Quadcopter Drone with HD Camera Optical Flow Positioning $43.99',
    link: 'https://www.gearbest.com/rc-helicopter-parts/pp_3006889583424188.html?lkid=79735552'
  },
  {
    slug: '5',
    title: 'GlobalDrone GD89 Foldable RC Drone $45.99',
    description: 'GlobalDrone GD89 Foldable RC Drone $45.99',
    link: 'https://www.gearbest.com/rc-quadcopters/pp_009883405788.html?lkid=79735598'
  },
  {
    slug: '6',
    title: 'JJRC H73 1080P 5G WiFi RC Drone RTF $82.99',
    description: 'JJRC H73 1080P 5G WiFi RC Drone RTF $82.99',
    link: 'https://www.gearbest.com/rc-quadcopters/pp_009791695228.html?lkid=79735587'
  },
  {
    slug: '7',
    title: 'VISUO XS809S WiFi FPV Camera RC Drone Quadcopter $66.54',
    description: 'VISUO XS809S WiFi FPV Camera RC Drone Quadcopter $66.54',
    link: 'https://www.gearbest.com/rc-quadcopters/pp_1827447.html?lkid=79735674'
  },
  {
    slug: '8',
    title: 'EMAX TINYHAWK 600TVL Camera Brushless Racing RC Drone $89.99',
    description: 'EMAX TINYHAWK 600TVL Camera Brushless Racing RC Drone $89.99',
    link: 'https://www.gearbest.com/brush-fpv-racer/pp_009662787547.html?lkid=79735684'
  },
  {
    slug: '9',
    title: 'JJRC X6 Aircus GPS RC Drone with Two-axis Stabilization PTZ Gimbal $165.99',
    description: 'JJRC X6 Aircus GPS RC Drone with Two-axis Stabilization PTZ Gimbal $165.99',
    link: 'https://www.gearbest.com/rc-quadcopters/pp_009164662742.html?lkid=79735680'
  },
  {
    slug: 'A',
    title: 'JJRC X11 GPS 5G WIFI FPV Foldable RC Drone Quadcopte with Wide Angle 2K Camera $189.99',
    description: 'JJRC X11 GPS 5G WIFI FPV Foldable RC Drone Quadcopte with Wide Angle 2K Camera $189.99',
    link: 'https://www.gearbest.com/rc-helicopter-parts/pp_3007273352045434.html?lkid=79735688'
  },
  {
    slug: 'B',
    title: 'JJRC X12 Foldable GPS 5G WIFI FPV RC Drone Helicopter Quadcopter with 1080P 4K HD Camera $249.99',
    description: 'JJRC X12 Foldable GPS 5G WIFI FPV RC Drone Helicopter Quadcopter with 1080P 4K HD Camera $249.99',
    link: 'https://www.gearbest.com/rc-helicopter-parts/pp_3008218569954531.html?lkid=79735692'
  },
  {
    slug: 'C',
    title: 'Hawkeye Firefly Micro 1080P Mini Action Camera $20.20',
    description: 'Hawkeye Firefly Micro 1080P Mini Action Camera $20.20',
    link: 'https://www.gearbest.com/action-cameras/pp_1588214.html?lkid=79735707'
  },
  {
    slug: 'D',
    title: 'Original SJCAM M20 2160P 16MP 166 Adjustable Degree WiFi Action Camera Sport DV Recorder $107.37',
    description: 'Original SJCAM M20 2160P 16MP 166 Adjustable Degree WiFi Action Camera Sport DV Recorder $107.37',
    link: 'https://www.gearbest.com/action-cameras/pp_314996.html?lkid=79735703'
  },
  {
    slug: 'E',
    title: 'Hawkeye Firefly 8SE 4K Touch Screen Action Camera $119.99',
    description: 'Hawkeye Firefly 8SE 4K Touch Screen Action Camera $119.99',
    link: 'https://www.gearbest.com/action-cameras/pp_009385918813.html?lkid=79735712'
  },
  {
    slug: 'F',
    title: 'Original SJCAM SJ7 STAR WiFi Action Camera 4K $199.21',
    description: 'Original SJCAM SJ7 STAR WiFi Action Camera 4K $199.21',
    link: 'https://www.gearbest.com/action-cameras/pp_482190.html?lkid=79735717'
  },
  {
    slug: '10',
    title: 'New Edition ThiEYE T5 Edge Live Stream Version Native 4K WiFi Action Camera $127.18',
    description: 'New Edition ThiEYE T5 Edge Live Stream Version Native 4K WiFi Action Camera $127.18',
    link: 'https://www.gearbest.com/action-cameras/pp_1390627.html?lkid=79735728'
  },
  {
    slug: '11',
    title: 'Insta360 ONE X 5.7K Panoramic Anti-shake Sports DV Action Camera $399.00',
    description: 'Insta360 ONE X 5.7K Panoramic Anti-shake Sports DV Action Camera $399.00',
    link: 'https://www.gearbest.com/action-cameras/pp_009506388192.html?lkid=79735732'
  },
  {
    slug: '12',
    title: 'Alfawise A80 2800 Lumens BD1280 Smart Projector with LCD Display $88.99',
    description: 'Alfawise A80 2800 Lumens BD1280 Smart Projector with LCD Display $88.99',
    link: 'https://www.gearbest.com/projectors/pp_009335543652.html?lkid=79735735'
  },
  {
    slug: '13',
    title: 'VIVIBRIGHT GP100 Projector $159.99',
    description: 'VIVIBRIGHT GP100 Projector $159.99',
    link: 'https://www.gearbest.com/projectors/pp_775859.html?lkid=79735740'
  },
  {
    slug: '14',
    title: 'RD - 606 DLP Mini Home Entertainment Projector $160.95',
    description: 'RD - 606 DLP Mini Home Entertainment Projector $160.95',
    link: 'https://www.gearbest.com/projectors/pp_009114345004.html?lkid=79735743'
  },
  {
    slug: '15',
    title: 'M18 LCD FHD Home Theater Projector $189.99',
    description: 'M18 LCD FHD Home Theater Projector $189.99',
    link: 'https://www.gearbest.com/projectors/pp_009925644118.html?lkid=79735747'
  },
  {
    slug: '16',
    title: 'Alfawise Q9 BD1080P HD 4K Smart Home Projector $179.99',
    description: 'Alfawise Q9 BD1080P HD 4K Smart Home Projector $179.99',
    link: 'https://www.gearbest.com/projectors/pp_009566925699.html?lkid=79735794'
  },
  {
    slug: '17',
    title: 'VIVIBRIGHT F30 LCD Projector Home Entertainment Commercial $175.99',
    description: 'VIVIBRIGHT F30 LCD Projector Home Entertainment Commercial $175.99',
    link: 'https://www.gearbest.com/projectors/pp_009408384762.html?lkid=79735797'
  },
  {
    slug: '18',
    title: 'XGIMI Z6 Smart Projector $529.99',
    description: 'XGIMI Z6 Smart Projector $529.99',
    link: 'https://www.gearbest.com/projectors/pp_009501735723.html?lkid=79735791'
  }
]
