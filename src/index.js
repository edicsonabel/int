import fs from 'node:fs'
import path from 'node:path'
import { URL } from 'node:url'
import data from './api/index.js'
import { copyRecursiveSync } from './utils/index.js'

const __dirname = new URL('.', import.meta.url).pathname

fs.rmSync(path.join(__dirname, '/../public/'), { force: true, recursive: true }, (err, other) => {
  if (err) {
    console.error(err)
  }
})
/* Creating pages */
fs.readFile(path.join(__dirname, '/partials/layaout.html'), 'utf8', (err, layaout) => {
  if (err) {
    console.error(err)
    return
  }

  /* looping through data */
  data.forEach(product => {
    const folderSlug = `/../public/${product.slug}`
    /* Create folder */
    fs.mkdir(path.join(__dirname, folderSlug), { recursive: true }, err => {
      if (err) {
        return console.error(err)
      }
      /* Create file HTML */
      let text = layaout
      text = text.replace(/%LINK%/g, product.link)
      text = text.replace(/%DESCRIPTION%/g, product.description)
      text = text.replace(/%TITLE%/g, product.title)
      fs.writeFile(path.join(__dirname, `${folderSlug}/index.html`), text, function (err) {
        if (err) throw err
      })
    })
  })

  const srcDir = path.join(__dirname, '/public')
  const destDir = path.join(__dirname, '/../public')

  copyRecursiveSync(srcDir, destDir)
  console.log('Pages completed')
})
